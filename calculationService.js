const calculationTimeInMillis = 20000;

const calculate = data => {
    console.log(`The calculation started for ${JSON.stringify(data)}`);

    const start = Date.now();
    let now = Date.now();

    while (true) {
        if (now - start > calculationTimeInMillis) {
            console.log(`The calculation have done for ${JSON.stringify(data)}`);
            return;
        }
        now = Date.now();
    }
};

module.exports = calculate;
