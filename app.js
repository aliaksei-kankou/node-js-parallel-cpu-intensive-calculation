const WorkerNodes = require('worker-nodes');
const path = require('path');
const express = require('express');
const app = express();

const calculationService = new WorkerNodes(path.join(__dirname, 'calculationService'));

app.use(express.json());

/**
 * Event Loop is not blocked by calculation because the calculation is performed in a separate thread
 */
app.get('/calculate/:id', (req, res) => {
    const id = req.params.id;
    calculate(req.params.id);
    res.send(`Calculation for ${id} has been started started...`);
});

const calculate = async (id) => {
    const data = { id: id };
    await calculationService.call(data);
};

const port = process.env.PORT || 9999;
app.listen(port, () => console.log(`The server has been started on ${port}`));
