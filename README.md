# Node JS. Performing CPU-intensive operations. #

### Performing CPU-intensive operations in separated thread  ###

This application simulates the execution of complex calculation (CPU-intensive) operations initiated by an http call.

In real life, these operations will block the Event Loop for the duration of one such operation and other users could not make their http requests.

Here is one way to solve this issue:

* Use Worker Threads

```
Workers (threads) are useful for performing CPU-intensive JavaScript operations. They will not help much with I/O-intensive work. Node.js’s built-in asynchronous I/O operations are more efficient than Workers can be.
```

When using Worker, the calculation starts in a separate thread, and the main thread (necessary for the event loop to work and process http requests) will not be blocked

* Another way to solve this issue could be to delegate performing to another application, lambda functions, etc.
